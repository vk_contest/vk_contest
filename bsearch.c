#include <math.h>
#include <time.h>
#include <stdio.h>
#include <stdlib.h>


int bsearch(int* array, int array_size, int x) {
	int l = 0, r = array_size - 1;
	for (; r > l; ) {
		int m = (l + r) / 2;
		if (array[m] > x) {
			r = m;
		} else {
			l = m + 1;
		}
	}
	if (array[r] > x) {
		return r; // Return index in zero-indexation.
	} else {
		return -1; // No element more than x.
	}
}


// TESTING SECTION.

const int SAMPLE_SIZE = 4;
const int MAX_NUMBER = 1000;
const int TESTS_N = 5;


int main(int argc, char* argv[]) {
	srand(time(NULL));
	int array[SAMPLE_SIZE];
	for (int i = 0; i < SAMPLE_SIZE; ++i) {
		array[i] = (i > 0 ? array[i - 1] : 0) + rand() % (MAX_NUMBER - (i > 0 ? array[i - 1] : 0));
	}
	for (int i = 0; i < SAMPLE_SIZE; ++i) {
		printf("%d ", array[i]);
	}
	printf("\n");
	for (int i = 0; i < TESTS_N; ++i) {
		int x = rand() % (MAX_NUMBER + 1);
		printf("%d %d\n", x, bsearch(array, SAMPLE_SIZE, x));
	}
	return 0;
}