#include <ctype.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>


const int MOD = (int)1e9 + 9;
const unsigned int SIZE = (int)1e7;
const int BASE = (int)1e9 + 7;

int* hash_array[SIZE];
int array_sizes[SIZE];

int HashIndex(int hash_str) {
   return hash_str % SIZE;
}

void Insert(int hash_str) {
    int hash_index = HashIndex(hash_str);
    int cur_size = array_sizes[hash_index];
    for (int i = 0; i < cur_size; ++i) {
        if (hash_array[hash_index][i] == hash_str) {
            return;
        }
        if (hash_array[hash_index][i] == -1) {
            hash_array[hash_index][i] = hash_str;
            return;
        }
    }
    hash_array[hash_index] = (int*)realloc(hash_array[hash_index], cur_size << 1);
    memset(hash_array[hash_index] + cur_size, -1, cur_size * sizeof(int));
    hash_array[hash_index][cur_size] = hash_str;
    array_sizes[hash_index] <<= 1;
}

void Build(const char* filename) {
    for (unsigned int i = 0; i < SIZE; ++i) {
        hash_array[i] = (int*)malloc(2 * sizeof(int));
        array_sizes[i] = 2;
        memset(hash_array[i], -1, 2 * sizeof(int));
    }
    FILE* file = fopen(filename, "r");
    if (file) {
        char c;
        int base_pow = 0, hash_str = 0;
        while ((c = fgetc(file)) != EOF) {
            if (isspace(c)) {
                if (base_pow) {
                    Insert(hash_str);
                    base_pow = 0, hash_str = 0;
                }
            } else {
                if (!base_pow) {
                    base_pow = 1;
                }
                hash_str += (base_pow * 1LL * c) % MOD;
                hash_str -= (hash_str >= MOD ? MOD : 0);
                base_pow = (base_pow * 1LL * BASE) % MOD;
            }
        }
    }
}

void Destroy() {
    for (unsigned int i = 0; i < SIZE; ++i) {
        free(hash_array[i]);
    }
}

int Search(int hash_str) {
    int hash_index = HashIndex(hash_str);
    for (int i = 0; i < array_sizes[hash_index]; ++i) {
        if (hash_array[hash_index][i] == hash_str) {
            return 1;
        }
    }
    return 0;
}

void Input() {
    const int CHARS_TO_READ = 5;
    char str[CHARS_TO_READ];
    int base_pow = 1, hash_str = 0;
    while (fgets(str, CHARS_TO_READ, stdin) != NULL) {
        if (strcmp(str, "exit") == 0) {
            break;
        }
        int len = strlen(str);
        if (str[len - 1] == '\n') {
            --len;
        }
        for (int i = 0; i < len; ++i) {
            char c = str[i];
            hash_str += (base_pow * 1LL * c) % MOD;
            hash_str -= (hash_str >= MOD ? MOD : 0);
            base_pow = (base_pow * 1LL * BASE) % MOD;
        }
        if (str[len] == '\n') {
            if (Search(hash_str)) {
                puts("YES");
            } else {
                puts("NO");
            }
            base_pow = 1, hash_str = 0;
        }
    }
}



int main(int argc, char* argv[]) {
    if (argc < 2) {
        fprintf(stderr, "No filename");
        return 0;
    }
    Build(argv[1]);
    Input();
    Destroy();
    return 0;
}